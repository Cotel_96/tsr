// Hello World client in Node.js
// Connects REQ socket to tcp://localhost:5559
// Sends "Hello" to server, expects "World" back

if(process.argv.length != 5) {
	console.log("usage: node lbclient.js [url] [id] [text]");
	process.exit(1);
}

var zmq       = require('zmq')
  , requester = zmq.socket('req')
  , frontend  = process.argv[2]
  , client    = process.argv[3]
  , msg       = process.argv[4];

requester.identity = client;
requester.connect(frontend);

requester.on('message', function(msg) {
  console.log('Respuesta recibida: ', msg.toString());
  process.exit();
});

requester.send(msg);


