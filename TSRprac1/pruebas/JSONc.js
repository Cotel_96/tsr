var net = require("net");

var msg = JSON.stringify({
	"name": "Miguel",
	"age": 20,
	"address": {
		"street": "Calle Falsa",
		"city": "Valencia"
	}
});

var socket = net.connect({port: 8000},
	function() {
		socket.write(msg)
	}
);
