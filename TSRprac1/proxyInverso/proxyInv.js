var net = require('net');

var PROG_PORT = 8000;
var LOCAL_IP = '127.0.0.1';
var LOCAL_PORTS = [8001, 8002, 8003, 8004, 8005, 8006, 8007, 8008];

var default_ips = ['158.42.184.5',
	'158.42.4.23', '216.58.210.163',
	'158.42.156.2', '147.156.222.65',
	'127.0.0.1', '127.0.0.1', '127.0.0.1' ];
var default_ports = [80, 80, 443, 80, 80, 80, 80, 80];

var servers = [];
var active_prog = 0;

for (var i=0; i<LOCAL_PORTS.length; i++) {
	servers[i] = net.createServer(function(x) {
		return function(socket) {
			socket.on("data", function(msg) {
				var serviceSocket = new net.Socket();

				serviceSocket.connect(default_ports[x], default_ips[x], function() {
					console.log(`Conectando a ${default_ips[x]}:${default_ports[x]} `);
					serviceSocket.write(msg);
				});

				serviceSocket.on("data", function(data) {
					socket.write(data);
				});
			});
		}

	}(i)).listen(LOCAL_PORTS[i], LOCAL_IP);
	console.log("Server initiated on %d", LOCAL_PORTS[i]);

}

var serverProg = net.createServer(function(socket) {
	socket.on("data", function(data) {
		var jData = JSON.parse(data);
		if(jData.op == 'set') {
			for(var i=0; i<LOCAL_PORTS.length; i++) {
				if (LOCAL_PORTS[i] == jData.inPort) {
					console.log(`Antes: ${jData.inPort} -> IP: ${default_ips[i]} PORT: ${default_ports[i]}`);
					default_ports[i] = jData.remote.port;
					default_ips[i] = jData.remote.ip;
					console.log(`Despues: ${jData.inPort} -> IP: ${default_ips[i]} PORT: ${default_ports[i]}`);
				}
			}
		}
	});
}).listen(PROG_PORT, LOCAL_IP);
