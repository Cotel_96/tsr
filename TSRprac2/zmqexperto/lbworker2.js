// Hello World server in Node.js
// Connects REP socket to tcp://*:5560
// Expects "Hello" from client, replies with "World"


if (process.argv.length != 7) {
  console.log("usage: node lbclient.js [backend_url] [heartbeat_url] [id] [text] [verbose (true/false)]");
  process.exit(1);
}

var zmq = require('zmq'),
  requester = zmq.socket('req'),
  heart = zmq.socket('req'),
  backend = process.argv[2] //direccion de broker
  ,
  heartp = process.argv[3] //direc del heartp
  ,
  worker = process.argv[4] //Identidad del server 
  ,
  att = process.argv[5] //variable que se responde
  ,
  verbose = process.argv[6]; //para saber si debe imprimir o no

requester.identity = worker;
requester.connect(backend);

heart.identity = worker;
heart.connect(heartp);

if(verbose) console.log("Enviando login...")
requester.send(JSON.stringify({
  id: worker,
  type: "LOGIN"
}));

requester.on('message', function () {
  var args = Array.apply(null, arguments);
  if(verbose) {
	console.log("Mensaje de broker: {");
	for(var i in args) {
	  console.log(args[i].toString());	
	}
	console.log("}");
  }
  requester.send(JSON.stringify({
    id: worker,
    type: "RESPONSE",
    data: [args[0].toString(), "", att]
  }));
});

heart.on('message', function (msg) {
  heart.send(JSON.stringify({
    id: worker,
    load: getLoad()
  })); //Devuelve la carga
  console.log('beep beep!', msg.toString());
});

function getLoad() {
  var fs = require('fs');
  data = fs.readFileSync('/proc/loadavg');
  tokens = data.toString().split(' '); //crea array de lo que hay en el documento
  min1 = parseFloat(tokens[0]) + 0.01; //coge un float y le suma 0.01
  min5 = parseFloat(tokens[1]) + 0.01; //coge un float y le suma 0.01
  min15 = parseFloat(tokens[2]) + 0.01; //coge un float y le suma 0.01
  m = min1 * 10 + min5 * 2 + min15; //Multiplica por diferentes bases y las suma 
  return m;
}
