var zmq = require('zmq');

if(process.argv.length != 5) {
	console.log("usage: node lbclient.js [url] [id] [text]");
	process.exit(1);
}

var url  = process.argv[2];
var id   = process.argv[3];
var text = process.argv[4];

var req = zmq.socket('req');

req.identity = id;
req.connect(url);

req.on("message", function(msg) {
	console.log("Response: " + msg.toString());
});

req.send(text);
