var zmq = require('zmq');

if(process.argv.length != 4) {
	console.log("Usage: node subscriber_2.js [url] [tipo]");
	process.exit(0);
}

var url = process.argv[2];
var tipo = process.argv[3];

var subscriber = zmq.socket('sub');

subscriber.on("message", function(rep) {
	console.log(`Received message ${rep.toString()}`);
});

subscriber.connect(url);
subscriber.subscribe(tipo);

process.on("SIGINT", function() {
	subscriber.close();
	console.log("\nClosed");
});
