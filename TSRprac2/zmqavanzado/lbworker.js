var zmq = require('zmq');

if(process.argv.length != 6) {
	console.log("usage: node lbclient.js [url] [id] [text] [verbose (true/false)]");
	process.exit(1);
}

var url     = process.argv[2];
var id      = process.argv[3];
var text    = process.argv[4];
var verbose = process.argv[5] == 'true';

var req = zmq.socket('req');

req.identity = id;
req.connect(url);

if(verbose) {
	console.log("Sending LOGIN request to broker...");
}
req.send(JSON.stringify({
	id: id,
	type: "LOGIN"
}));

req.on("message", function() {
	var args = Array.apply(null, arguments);
	if(verbose) console.log("Message arrived: [%s, %s]", args[0], args[2]);
	if(verbose) console.log("Sending reponse...");
	req.send(JSON.stringify({
		id: id,
		type: "RESPONSE",
		data: [args[0].toString(), "", text]
	}));	
});
