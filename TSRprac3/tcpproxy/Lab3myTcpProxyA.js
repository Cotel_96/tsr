var net = require('net');

var LOCAL_PORT = 80;
var LOCAL_IP = '127.0.0.1';

if (process.argv.length != 4) {
	console.error("The wordpress port and IP should be given as args");
	process.exit(1)
}

var REMOTE_PORT = process.argv[2];
var REMOTE_IP = process.argv[3];

var server = net.createServer(function(socket) {
	var serviceSocket = new net.Socket();
	serviceSocket.connect(parseInt(REMOTE_PORT), REMOTE_IP, function() {
		socket.on('data', function(msg) {
			serviceSocket.write(msg);
		});
		serviceSocket.on('data', function(data) {
			socket.write(data);
		});
	});
}).listen(LOCAL_PORT, LOCAL_IP);
console.log("TCP server on " + LOCAL_PORT);
