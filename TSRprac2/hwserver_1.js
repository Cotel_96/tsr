var zmq = require("zmq");

if(process.argv.length != 5) {
	console.log("Usage: node hwserver_1.js [port] [times] [text]");
	process.exit(0);
}

var port = process.argv[2];
var times = process.argv[3];
var text = process.argv[4];

var responder = zmq.socket("rep");

responder.on("message", function(req) {
	console.log(`Received request: ${req.toString()}`);
	setTimeout(function() {
		responder.send(req.toString()+" "+text);
	}, times*1000);
});

responder.bind("tcp://*:"+port, function(err) {
	if(err) {
		console.log(err);
	} else {
		console.log("Listening on "+port);
	}
});

process.on("SIGINT", function() {
	responder.close();
});
