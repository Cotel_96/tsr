var net = require("net");

var server = net.createServer(
	function(c) {
		console.log("Server: client connected");
		c.on("end",
			function() {
				console.log("Server: client disconnected");
			}
		);
		c.on("data",
			function(data) {
				var person = JSON.parse(data);
				console.log(person.name);
				console.log(person.age);
			}
		);
	}
);
	
server.listen(8000,
	function() {
		console.log("Server bound");
	}
);
