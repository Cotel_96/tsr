var zmq = require('zmq');
var subscriber = zmq.socket('sub');

subscriber.on("message", function(rep) {
	console.log(`Received message ${rep.toString()}`);
});

subscriber.connect("tcp://localhost:8688");
subscriber.subscribe("");

process.on("SIGINT", function() {
	subscriber.close();
	console.log("\nClosed");
});
