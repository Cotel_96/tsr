var zmq = require("zmq");

if(process.argv.length != 5) {
	console.log("Usage: node rrworker.js [url] [time] [text]");
	process.exit(0);
}

var url = process.argv[2];
var t = process.argv[3];
var text = process.argv[4];

var responder = zmq.socket("req");

responder.connect(url);

responder.on("message", function(msg) {
	console.log(`Received request: ${msg.toString()}`);
	setTimeout(function() {
		responder.send(text);
	}, t*1000);
});
