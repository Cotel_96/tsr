// Simple request-reply broker in Node.js
const ENABLED = 1;
const BUSY = 0;

if (process.argv.length != 6) {
	console.log("usage: node lbbroker.js [frontend_port] [backend_port] [heartbeat_port] [verbose: (true/false)]");
	process.exit(1);
}

var zmq = require('zmq'),
	frontend = zmq.socket('router') //cliente
	,
	backend = zmq.socket('router') //server
	,
	fp = process.argv[2] //puerto cliente
	,
	bp = process.argv[3] //puerto server
	,
	hp = process.argv[4] //puesto corasonsito
	,
	verbose = process.argv[5] == 'true',
	heart = zmq.socket('router'); //controlador

var identidades_trabajador = [];
var peticiones = [];

frontend.bindSync('tcp://*:' + fp);
backend.bindSync('tcp://*:' + bp);
heart.bindSync('tcp://*:' + hp);


frontend.on('message', function () {
	// Note that separate message parts come as function arguments.
	var args = Array.apply(null, arguments);
	if (verbose) console.log('Petición recibida.');
	// Pass array of strings/buffers to send multipart messages.

	var sent = false;
	for (var worker of identidades_trabajador) {
		if(verbose) console.log("Servidor actual: " + worker.id + " " + worker.status)
		if (worker.status == ENABLED) {
			if (verbose) console.log("Encontrado servidor activo con id: ", worker.id);
			if (peticiones.length != 0) {
				if (verbose) console.log("Procesando cola de mensajes");
				backend.send([worker.id, "", peticiones[0]]);
				peticiones.shift();
			} else {
				args.unshift("");
				args.unshift(worker.id);
				backend.send(args);
				sent = true;
			}
			worker.status == BUSY;
			break;
		}
	}

	if (!sent) {
		if (verbose) console.log("Ningun servidor disponible, encolando mensaje...");
		peticiones.push(args);
	}

});

backend.on('message', function () { //Cuando recibe del server
	var args = Array.apply(null, arguments);
	if(verbose)  {
		console.log("Mensaje de servidor: ")
		for (var i in args) {
			console.log(args[i].toString()); //imprime todo el objeto
		}
	}

	var jData = JSON.parse(args[2]);

	switch (jData.type) {
		case "LOGIN":
			if(verbose) console.log("Registrando servidor...")
			identidades_trabajador.push({
				id: jData.id,
				status: ENABLED,
				times_dead: 0
			});
			break;
		case "RESPONSE":
			if (verbose) console.log("Mandando respuesta al cliente: ", jData.data);
			frontend.send(jData.data);
			for (var w of identidades_trabajador) {
				if (w.id == jData.id) {
					w.status = ENABLED;
					w.times_dead = 0;
					break;
				}
			}
			break;
	}
});

heart.on('message', function () {
	var args = Array.apply(null, arguments);
	if (verbose) {
		console.log("Mensaje de heartbeat: ")
		for (var i in args) {
			console.log(args[i].toString());
		}
	}

	var jData = JSON.parse(args[2]);

	var carga = jData.load;
	var worker = jData.id;

	for (var w of identidades_trabajador) {
		if(w.id == worker) {
			if(verbose) console.log("Reviviendo servidor...")
			w.load = carga;
			w.times_dead = 0;
			break;
		}
	}

	identidades_trabajador.sort(function(worker, nextWorker) {
		return worker.load <= nextWorker.load
	})
});


var e1 = function () {

	for(var i in identidades_trabajador) {
		if(identidades_trabajador[i].times_dead < 5) {
			identidades_trabajador[i].times_dead++
			heart.send(identidades_trabajador[i], null);
		} else {
			if(verbose) console.log("Matando al servidor ", identidades_trabajador[i].id)
			identidades_trabajador.splice(i, 1);
		}
	}
	heart.send(null);
}

setInterval(e1, 3000);
