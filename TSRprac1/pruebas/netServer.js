var net = require("net");

var server = net.createServer(
	function(c) {
		console.log("Server: client connected");
		console.log(c);
		c.on("end",
			function() {
				console.log("Server: client disconnected");
			}
		);
		c.on("data",
			function(data) {
				c.write("Hello\r\n"+data.toString());
				c.end();
			}
		);
	}
);

server.listen(8000,
	function() {
		console.log("Server bound");
	}
);
