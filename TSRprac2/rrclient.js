var zmq = require("zmq");

if(process.argv.length != 6) {
	console.log("Usage: node rrclient.js [url] [id] [num] [text]");
	process.exit(0);
}

var url = process.argv[2];
var id = process.argv[3];
var num = process.argv[4];
var text = process.argv[5];


var requester = zmq.socket("req");
requester.identity = id;
requester.connect(url);
var replyNbr = 0;

requester.on("message", function(msg) {
	console.log(`Got reply ${replyNbr} ${msg.toString()}`);
	replyNbr += 1;
});

for (var i=0; i<num; ++i) {
	requester.send(text);
}
