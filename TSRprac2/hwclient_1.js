var zmq = require('zmq');

if(process.argv.length != 5) {
	console.log("Usage: node hwclient_1.js [url] [times] [text]");
	process.exit(0);
}

var url = process.argv[2];
var times = process.argv[3];
var text = process.argv[4];


console.log("Connecting to hello world server...");
var requester = zmq.socket('req');

var x = 0;
requester.on("message", function(reply) {
	console.log(`Received repy ${x} : [${reply.toString()}]`);
	x += 1;
	if (x==10) {
		requester.close();
		process.exit(0);
	}
});

requester.connect(url);

for (var i=0; i<times; i++) {
	console.log("Sending request "+i+" ...");
	requester.send(text);
}

process.on("SIGINT", function() {
	requester.close();
});
