var zmq = require("zmq");

if (process.argv.length != 6) {
	console.log("Usage: node publisher_2.js [port] [nº msgs] [type1] [type2]");
	process.exit(0);
}

var port = process.argv[2];
var msgs = process.argv[3];
var tipe1 = process.argv[4];
var tipe2 = process.argv[5];


var publisher = zmq.socket("pub");

publisher.bind("tcp://*:"+port, function(err) {
	if (err) {
		console.log(err);
	} else {
		console.log("Listening on" + port);
	}
});

for (var i=1; i<=msgs; i++) {
	setTimeout(function() {
		console.log("sent");
		publisher.send(tipe1+": Hello there! "+tipe1);
		publisher.send(tipe2+": Hello there! "+tipe2);
	}, 1000 * i);
}

process.on("SIGINT", function() {
	publisher.close();
	console.log("\nClosed");
});
