var zmq = require("zmq");

if(process.argv.length != 4) {
	console.log("Usage: node rrbroker.js [client port] [server port]");
	process.exit(0);
}

var cport = process.argv[2];
var sport = process.argv[3];

var frontend = zmq.socket("router");
var backend = zmq.socket("dealer");

frontend.bindSync("tcp://*:"+cport);
backend.bindSync("tcp://*:"+sport);

frontend.on("message", function() {
	var args = Array.apply(null, arguments);
	backend.send(args);
});

backend.on("message", function() {
	var args = Array.apply(null, arguments);
	frontend.send(args);
});
