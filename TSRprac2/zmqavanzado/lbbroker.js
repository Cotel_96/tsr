var zmq = require('zmq');

const ENABLED = 1;
const BUSY = 0;

if(process.argv.length != 5) {
	console.log("usage: node lbbroker.js [frontend_port] [backend_port] [verbose: (true/false)]");
	process.exit(1);
}

var workers = [];

var frontend = zmq.socket('router');
var backend  = zmq.socket('router');

var f_port  = process.argv[2];
var b_port  = process.argv[3];
var verbose = process.argv[4] == 'true';

if(verbose) console.log("Setting up frontend in port: ", f_port);
frontend.bindSync("tcp://*:"+f_port);
if(verbose) console.log("Setting up backend in port: ", b_port);
backend.bindSync("tcp://*:"+b_port);

frontend.on("message", function() {	
	var args = Array.apply(null, arguments);
	if(verbose) console.log("Got frontend message: [%s, %s]", args[0], args[2]);
	for(var w of workers) {
		if(w.status == ENABLED) {
			if(verbose) console.log("Found worker active with id: ", w.id);
			args.unshift("");
			args.unshift(w.id);
			if(verbose) console.log("Sending message to worker: [%s,%s,%s]", args[0], args[2], args[4]);
			backend.send(args);
			w.status = BUSY;
			break;
		}
	}
});

backend.on("message", function() {
	var args = Array.apply(null, arguments);
	if(verbose) console.log("Got backend message: ", args[2].toString());
	var jData = JSON.parse(args[2]);
	
	switch(jData.type) {
		case "LOGIN":
			workers.push({
				id: jData.id,
				status: ENABLED
			});
			break;
		case "RESPONSE":
			if(verbose) console.log("Sending back to client: ", jData.data);
			frontend.send(jData.data);
			for(var w of workers) {
				if(w.id == jData.id) {
					w.status = ENABLED;
					break;
				}
			}
			break;
	}
	
	
});
